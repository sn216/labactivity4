//Sriraam Nadarajah
//2245615
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;


public class Vector3dTests 
{
    final double TOLERANCE = 0.0001;
   
    @Test
    public void testGetters_returns2()
    {
        Vector3d vector = new Vector3d(2.0, 2.0, 2.0);
        assertEquals(2.0,vector.getX(), TOLERANCE);
        assertEquals(2.0,vector.getY(), TOLERANCE);
        assertEquals(2.0,vector.getZ(), TOLERANCE);
    }

    @Test
    public void testMagnitude_returns3()
    {
        Vector3d vector = new Vector3d(1.0, 1.0, 1.0);
        assertEquals(1.732,vector.magnitude(),TOLERANCE);
    }

    @Test
    public void testDotProduct_returns32()
    {
        Vector3d firstVector = new Vector3d(1.0, 2.0, 3.0);
        Vector3d secondVector = new Vector3d(4.0, 5.0, 6.0);
        assertEquals(32.0,firstVector.dotProduct(secondVector), TOLERANCE);
    }

    @Test
    public void testAdd_returns2_5_7(){
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(1.0, 3.0, 4.0);
        Vector3d result = vector1.add(vector2);

        assertEquals(2.0, result.getX(), TOLERANCE);
        assertEquals(5.0, result.getY(), TOLERANCE); 
        assertEquals(7.0, result.getZ(), TOLERANCE);
    }
}