//Sriraam Nadarajah
//2245165
package linearalgebra;

public class Vector3d 
{
    private double x;
    private double y;
    private double z;

    public Vector3d (double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double magnitude(){
        return Math.sqrt((Math.pow(this.x , 2)) + (Math.pow(this.y , 2)) + (Math.pow(this.z, 2)) );
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }
    
    public double dotProduct(Vector3d secondVector){
        return (this.x * secondVector.getX()) + (this.y * secondVector.getY()) + (this.z * secondVector.getZ());
    }

    public Vector3d add(Vector3d secondVector){
        double newX = this.x + secondVector.getX();
        double newY = this.y + secondVector.getY();
        double newZ = this.z + secondVector.getZ();

        return new Vector3d(newX, newY, newZ);
    }
}